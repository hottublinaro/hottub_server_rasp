from urllib.request import urlopen
import json
from connect_db import Connect_db
from path_url import Path_URL
import requests


connect_db = Connect_db
path_file = Path_URL


url = path_file.path_local+"api/Rest_api/get_dashborad"
url_server = path_file.path_server+'api/Receive_api/save_dashboard_online_pi4' 

class Setting_dashboard:

    def get_setting(machine_code, dt_string):
         with open('/home/pi/txt_file/pressure.txt','r') as read_pressure:
            presure = read_pressure.readline().strip()
            read_pressure.close()
         with open('/home/pi/txt_file/ph.txt','r') as read_ph:
            ph = read_ph.readline().strip()
            read_ph.close()
         with open('/home/pi/txt_file/orp.txt','r') as read_orp:
            orp = read_orp.readline().strip()
            read_orp.close()
         with open('/home/pi/txt_file/temperature.txt','r') as read_temperature:
            temperature = read_temperature.readline().strip()
            read_temperature.close()
         with open('/home/pi/txt_file/status_plc.txt','r') as read_plc_out:
            plc_out = read_plc_out.readline().strip()
            eval_plc_out = eval(plc_out)
            read_plc_out.close()
         with open('/home/pi/txt_file/status_plc_in.txt','r') as read_plc_in:
            plc_in = read_plc_in.readline().strip()
            eval_plc_in = eval(plc_in)
            read_plc_in.close()
         with open('/home/pi/txt_file/status_relay_8.txt','r') as read_relay:
            relay_8 = read_relay.readline().strip()
            eval_relay_8 = eval(relay_8)
            read_relay.close()

         with open('/home/pi/txt_file/volt_tag.txt','r') as read_volt:
            volt = read_volt.readline().strip()
            split_volt = volt.split(',')
            read_volt.close()
         with open('/home/pi/txt_file/counter_ja.txt','r') as read_counter_ja:
            counter_ja = read_counter_ja.readline().strip()

         with open('/home/pi/txt_file/status_plc_in_16.txt','r') as read_all_in_16:
            plc_all_in_16 = read_all_in_16.readline().strip()
            eval_plc_all_in_16 = eval(plc_all_in_16)
            read_all_in_16.close()
            
         resp = requests.post(url_server, data={'datetime':dt_string,
                                             'pressure': presure,
                                             'ph':ph,
                                             'orp':orp,
                                             'temperature':temperature,
                                             'filtration':eval_plc_out[0],
                                             'pompe_ozone':eval_plc_out[1],
                                             'chauffage':eval_plc_out[2],
                                             'chauffage2':eval_plc_out[3],
                                             'lamp_zone1':eval_plc_in[3],
                                             'lamp_zone2':eval_plc_in[4],
                                             'lamp_uv':eval_plc_in[5],
                                             'volt_1':split_volt[0],
                                             'volt_2':split_volt[1],
                                             'volt_3':split_volt[2],
                                             'machine_code':machine_code,
                                             'max_pressure':0,
                                             'min_pressure':0,
                                             "plc_in":plc_in,
                                             "plc_out":plc_out,
                                             "relay_ch":relay_8,
                                             "count_down":0,
                                             "counter_ja": counter_ja,
                                             "all_in_plc_16" : eval_plc_all_in_16
                                          })
         print(resp)

       
        # data_json[0]['sm_filtration']
